# cpnv-lin1-scripts

### Check BIND9 zones
```console
cpnv@SRV-LIN1-01:~# named-checkzone lin1.local /etc/bind/zones/db.lin1.local
cpnv@SRV-LIN1-01:~# named-checkzone 10.10.10.in-addr.arpa /etc/bind/zones/db.10.10.10
cpnv@SRV-LIN1-01:~# dig NS lin1.local @127.0.0.1
cpnv@SRV-LIN1-01:~# dig -x 10.10.10.11 @127.0.0.1
```

### OpenLDAP users password
```yaml
Password: Bo0avwpwOI
```

### Generate RSA SSH key pair and use it
```console
cpnv@client:~# ssh-keygen -o -a 322 -t rsa -f ./lin1-rsa -C "lin1.local"
cpnv@client:~# ssh-copy-id -i ./lin1-rsa.pub cpnv@10.10.10.11
cpnv@client:~# ssh -i ./lin1-rsa cpnv@10.10.10.11
```
