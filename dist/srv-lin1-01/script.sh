#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

if [[ "${TRACE-0}" == "1" ]]; then
	set -o xtrace
fi

source ./config.conf

if [[ -z "${SERVER_HOSTNAME}" ]]; then
	echo "Cannot read config.conf" 
	exit 1
fi


set_hostname () {
	echo "${SERVER_HOSTNAME}" > /etc/hostname
}

set_apt_sources_list () {
/usr/bin/cat << EOF > /etc/apt/sources.list
deb https://debian.ethz.ch/debian/ bullseye main contrib non-free
deb https://debian.ethz.ch/debian/ bullseye-updates main contrib non-free
deb https://security.debian.org/debian-security bullseye-security main contrib non-free
EOF
	apt-get update
}

install_base_packages () {
	apt-get update
	apt-get install -y ${BASE_PACKAGES}
}

install_slapd () {
	local bind_user="admin"
	local base_dn_lock="/var/lock/.basedn"
	local base_users_groups_lock="/var/lock/.base_users_groups"
	local memberof_lock="/var/lock/.memberof"

	echo slapd slapd/password2 password "${SLAPD_ADMIN_PASSWORD}" | debconf-set-selections
	echo slapd slapd/password1 password "${SLAPD_ADMIN_PASSWORD}" | debconf-set-selections
	echo slapd slapd/domain string "${SLAPD_DOMAIN}.${SLAPD_TLD}" | debconf-set-selections
	echo slapd shared/organization string "${SLAPD_ORGANIZATION}" | debconf-set-selections
	echo slapd slapd/purge_database boolean "${SLAPD_PURGE_DATABASE}" | debconf-set-selections
	echo slapd slapd/move_old_database boolean "${SLAPD_MOVE_OLD_DATABASE}" | debconf-set-selections

	apt-get install -y slapd ldap-utils
	

	if [[ ! -f "${memberof_lock}" ]]; then
		ldapadd -Y EXTERNAL -H ldapi:/// -f ./configs/slapd/update-memberof.ldif
		ldapadd -Y EXTERNAL -H ldapi:/// -f ./configs/slapd/add-memberof-overlay.ldif
		ldapadd -Y EXTERNAL -H ldapi:/// -f ./configs/slapd/add-refint.ldif
		touch "${memberof_lock}"
	fi
	if [[ ! -f "${base_dn_lock}" ]]; then
		ldapadd -x -D cn="${bind_user}",dc="${SLAPD_DOMAIN}",dc="${SLAPD_TLD}" -w "${SLAPD_ADMIN_PASSWORD}" -c -f ./configs/slapd/basedn.ldif
		touch "${base_dn_lock}"
	fi
	if [[ ! -f "${base_users_groups_lock}" ]]; then
		ldapadd -x -D cn="${bind_user}",dc="${SLAPD_DOMAIN}",dc="${SLAPD_TLD}" -w "${SLAPD_ADMIN_PASSWORD}" -c -f ./configs/slapd/base_users_groups.ldif
		touch "${base_users_groups_lock}"
	fi
}

install_dhcpd () {
	local dhcpd_dir="/etc/dhcp"

	apt-get install -y isc-dhcp-server --no-install-recommends

	cp ./configs/dhcpd/etc/default/isc-dhcp-server /etc/default/isc-dhcp-server
	cp ./configs/dhcpd/etc/dhcp/dhcpd.conf "${dhcpd_dir}"/dhcpd.conf

	chmod 664 {"${dhcpd_dir}"/dhcpd.conf,/etc/default/isc-dhcp-server}

	systemctl enable --now isc-dhcp-server
}

install_bind9 () {
	local bind_dir="/etc/bind"
	local bind_dir_zones="${bind_dir}/zones"

	apt-get install -y bind9 bind9utils --no-install-recommends

	mkdir -p "${bind_dir_zones}"

	cp ./configs/bind9/etc/bind/named.conf.options "${bind_dir}"/named.conf.options
	cp ./configs/bind9/etc/bind/named.conf.local "${bind_dir}"/named.conf.local
	cp ./configs/bind9/etc/bind/zones/db.10.10.10 "${bind_dir_zones}"/db.10.10.10
	cp ./configs/bind9/etc/bind/zones/db.lin1.local "${bind_dir_zones}"/db.lin1.local
	cp ./configs/bind9/etc/bind/zones/db.lin1.local "${bind_dir_zones}"/db.lin1.local
	cp ./configs/bind9/etc/default/named /etc/default/named
	
	chmod 664 "${bind_dir}"/named.conf.{options,local}
	chmod 664 /etc/default/named

	systemctl restart named
}

main () {
	set_hostname
	install_base_packages
	set_apt_sources_list
	install_bind9
	install_dhcpd
	install_slapd
}

main "${@}"
