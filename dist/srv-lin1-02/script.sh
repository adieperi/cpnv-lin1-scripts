#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

if [[ "${TRACE-0}" == "1" ]]; then
	set -o xtrace
fi

source ./config.conf

if [[ -z "${SERVER_HOSTNAME}" ]]; then
	echo "Cannot read config.conf" 
	exit 1
fi


set_hostname () {
	echo "${SERVER_HOSTNAME}" > /etc/hostname
}

set_apt_sources_list () {
	/usr/bin/cat << EOF > /etc/apt/sources.list
deb https://debian.ethz.ch/debian/ bullseye main contrib non-free
deb https://debian.ethz.ch/debian/ bullseye-updates main contrib non-free
deb https://security.debian.org/debian-security bullseye-security main contrib non-free
EOF
	apt-get update
}

install_base_packages () {
	apt-get update
	apt-get install -y ${BASE_PACKAGES}
}

install_owncloud () {
	local www_user_id="33"
	local www_user_name="www-data"
	local owncloud_directory="/var/www/html"
	local owncloud_dl_url="https://download.owncloud.com/server/stable/owncloud-complete-latest.tar.bz2"
	local owncloud_lock="/var/lock/.owncloud"
	local owncloud_db_type="sqlite"
	local owncloud_data_directory="/mnt/owncloud_data"
	local owncloud_config_file="${owncloud_directory}/config/config.php"
	local trusted_domain="srv-lin1-02.lin1.local"
	local trusted_ip="10.10.10.22"
	local nfs_export="10.10.10.33:/export/owncloud_data"
	local owncloud_version
	local owncloud_instanceid
	local owncloud_secret
	local owncloud_passwordsalt


	if [[ ! -f "${owncloud_lock}" ]]; then
		apt-get install -y apache2 php7.4 php7.4-{common,curl,gd,xml,zip,sqlite3,ldap,intl,mbstring}
		curl -o /tmp/owncloud-complete-latest.tar.bz2 "${owncloud_dl_url}"
		tar -xjf /tmp/owncloud-complete-latest.tar.bz2 --directory /tmp/
		rsync -ahc /tmp/owncloud/ "${owncloud_directory}"
		chown "${www_user_id}":"${www_user_id}" -R "${owncloud_directory}"
		systemctl restart apache2
		mkdir -p "${owncloud_data_directory}"
		mount -t nfs "${nfs_export}" "${owncloud_data_directory}"

		if ! cat /etc/fstab | grep "${owncloud_data_directory}"; then
			echo "${nfs_export} ${owncloud_data_directory} nfs defaults 0 0" 1>>/etc/fstab
		fi

		chown "${www_user_id}":"${www_user_id}" -R "${owncloud_data_directory}"

		sudo -u "${www_user_name}" "${owncloud_directory}/occ" maintenance:install \
			--database "${owncloud_db_type}" \
			--admin-user "${OWNCLOUD_ADMIN_USER}" \
			--admin-pass "${OWNCLOUD_ADMIN_PASSWORD}" \
			--data-dir "${owncloud_data_directory}"

		sudo -u "${www_user_name}" "${owncloud_directory}/occ" market:install user_ldap
		sudo -u "${www_user_name}" "${owncloud_directory}/occ" market:install calendar

		owncloud_version=$(/usr/bin/cat "${owncloud_config_file}" | grep version | awk '{print $3}' | sed 's/\,//g')
		owncloud_instanceid=$(/usr/bin/cat "${owncloud_config_file}" | grep instanceid | awk '{print $3}' | sed 's/\,//g')
		owncloud_secret=$(/usr/bin/cat "${owncloud_config_file}" | grep secret | awk '{print $3}' | sed 's/\,//g')
		owncloud_passwordsalt=$(/usr/bin/cat "${owncloud_config_file}" | grep passwordsalt | awk '{print $3}' | sed 's/\,//g')

		/usr/bin/cat << EOF > "${owncloud_config_file}"
<?php
\$CONFIG = array (
  'passwordsalt' => ${owncloud_passwordsalt},
  'secret' => ${owncloud_secret},
  'trusted_domains' => 
  array (
    0 => 'localhost',
    1 => '${trusted_domain}',
    2 => '${trusted_ip}',
  ),
  'datadirectory' => '${owncloud_data_directory}',
  'overwrite.cli.url' => 'http://localhost',
  'dbtype' => 'sqlite3',
  'version' => ${owncloud_version},
  'allow_user_to_change_mail_address' => '',
  'logtimezone' => 'UTC',
  'files_external_allow_create_new_local' => 'true',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/apps-external',
      'url' => '/apps-external',
      'writable' => true,
    ),
  ),
  'installed' => true,
  'instanceid' => ${owncloud_instanceid},
  'ldapIgnoreNamingRules' => false,
);
EOF
		sudo -u "${www_user_name}" mkdir "${owncloud_data_directory}/clients"
		sudo -u "${www_user_name}" mkdir "${owncloud_data_directory}/logiciels"
		sudo -u "${www_user_name}" mkdir "${owncloud_data_directory}/commun"
		touch "${owncloud_lock}"
	fi
}

main () {
	set_hostname
	install_base_packages
	set_apt_sources_list
	install_owncloud
}

main "${@}"
